<?php

/**
 * @file
 * Describe the entity with interface for managing bundles.
 */

/**
 * Implements hook_permission().
 */
function soundtrack_permission() {
  $perms = array(
    'create soundtrack content' => array(
      'title' => t('View Soundtrack content'),
    ),
    'view own soundtrack content' => array(
      'title' => t('View Soundtrack content'),
    ),
    'view any soundtrack content' => array(
      'title' => t('View Soundtrack content'),
    ),
    'edit own soundtrack content' => array(
      'title' => t('View Soundtrack content'),
    ),
    'edit any soundtrack content' => array(
      'title' => t('View Soundtrack content'),
    ),
    'delete own soundtrack content' => array(
      'title' => t('View Soundtrack content'),
    ),
    'delete any soundtrack content' => array(
      'title' => t('View Soundtrack content'),
    ),
    'administer soundtrack content' => array(
      'title' => t('Administer Soundtrack content'),
      'restrict access' => TRUE,
    ),
    'administer soundtrack types' => array(
      'title' => t('Administer Soundtrack types'),
      'restrict access' => TRUE,
    ),
  );
  return $perms;
}

/**
 * Implements hook_entity_info().
 */
function soundtrack_entity_info() {
  $info = array(
    'soundtrack' => array(
      'label' => t('Soundtrack'),
      'base table' => 'soundtrack',
      'entity keys' => array(
        'id' => 'id',
        'bundle' => 'type',
      ),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'bundles' => array(),
      // 'entity class' => 'Soundtrack',
      // 'controller class' => 'SoundtrackController',
      'metadata controller class' => '',
      'fieldable' => TRUE,
      'uri callback' => 'entity_class_uri',
      'label callback' => 'entity_class_label',
      'module' => 'soundtrack',
    ),
  );
  $info['soundtrack_type'] = array(
    'label' => t('Soundtrack Type'),
    'base table' => 'soundtrack_type',
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'bundle of' => 'soundtrack',
    // 'entity class' => 'SoundtrackType',
    // 'controller class' => 'SoundtrackTypeController',
    'metadata controller class' => '',
    'fieldable' => FALSE,
    'exportable' => TRUE,
    'module' => 'soundtrack',
    // Automatically create page for managing entity bundles.
    'admin ui' => array(
      'path' => 'admin/structure/soundtrack',
      'file' => 'soundtrack.admin.inc',
      // 'controller class' => 'SoundtrackTypeUiController',
    ),
    'access callback' => 'soundtrack_type_access',
  );
  return $info;
}

/**
 * Implements hook_menu().
 */
function soundtrack_menu() {
  $items = array();

  // Base URI parameters.
  $base_uri = 'soundtrack/%soundtrack';
  $base_uri_argument_position = 1;

  // Admin URI parameters.
  $admin_uri = 'admin/soundtrack/%soundtrack_type';
  $admin_uri_argument_position = 2;

  // URI for viewing list of existing entities.
  $items['admin/soundtrack'] = array(
    'title' => 'Soundtrack',
    'page callback' => 'soundtrack_type_list',
    'file' => 'soundtrack.admin.inc',
    'access arguments' => array('administer soundtrack types'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Access callback for soundtrack_type.
 */
function soundtrack_type_access($op, $entity = NULL) {
  return user_access('administer soundtrack content');
}
